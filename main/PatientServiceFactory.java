package main;

import java.util.Properties;

public  abstract class PatientServiceFactory {
	public static PatientService getPatientService(Properties properties)
	{
		String type=properties.getProperty("implementation");
		type=type.toUpperCase();
		PatientService service=null;
		switch(type)
		{
		case "ARRAY":
			service=new PatientServiceImpl();
			break;
			
		case "ARRAYLIST":
			service=new PatientServiceArrayListImpl();
			break;
			
		case "DATABASE":
			
			String driver=properties.getProperty("driver");
			String url=properties.getProperty("url");
			String user=properties.getProperty("user");
			String password=properties.getProperty("password");
			service=new PatientServiceDatabaseImpl(driver,url,user,password);
			break;
			
			
		}
		return service;
	}
	

}
