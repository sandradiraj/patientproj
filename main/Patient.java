package main;

import java.io.PrintStream;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Scanner;

import main.Gender;
import main.Patient;

public class Patient implements  Comparable<Patient> 
{
	private String name;
	private int age;
	private String doctorName;
	private Gender gender;
	LocalDate dateOfBirth;
	
	
	public void getDatas(Scanner sn)
	{
		System.out.println("Enter your name  :");
	    name=sn.next();
	    System.out.println("Enter your age :");
		age=sn.nextInt();
		System.out.println("Enter the doctors name :");
		doctorName=sn.next();
		System.out.println("Enter your gender :");
		gender=Gender.valueOf(sn.next().toUpperCase());
		System.out.println("Enter your age :");
		age=sn.nextInt();
		
	}
	public Patient(String name, int age, String doctorName, Gender gender, LocalDate dateOfBirth) {
	
		this.name = name;
		this.age = age;
		this.doctorName = doctorName;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		
	}
	
	
	
	public void display()
	{
		
		System.out.println("NAME : " +name);
		System.out.println();
		System.out.println("AGE  : " +age);
		System.out.println();
		System.out.println("Doctors Name : " +doctorName);
		System.out.println();
		System.out.println("GENDER : " +gender);
		System.out.println();
		System.out.println("DOB : " +dateOfBirth);
		
	}

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String toString()

	{
		
		return  new StringBuffer("NAME :  ").append(name).append("\nAGE : ").append(age).append("\nDOCTOR NAME : ")
				.append(doctorName).append("\nGENDER : ").append(gender).append("\nDATE OF BIRTH : ").append(dateOfBirth).toString();
				
		
		
	}
	
	


public int compareTo(Patient patient) 
{
	// TODO Auto-generated method stub
	
	return age-patient.age;
	
	
	
}
public Patient() {
	// TODO Auto-generated constructor stub
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Patient other = (Patient) obj;
	return age == other.age && Objects.equals(dateOfBirth, other.dateOfBirth) && Objects.equals(doctorName, other.doctorName)
			&& gender == other.gender  && Objects.equals(name, other.name);
}
}




	



