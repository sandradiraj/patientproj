package main;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringJoiner;

public class PatientServiceDatabaseImpl implements PatientService 
{
      String url,driver,user,password;

	public PatientServiceDatabaseImpl(String driver, String url, String user, String password) 
	{
		
		this.driver = driver;
		this.url = url;
		this.user = user;
		this.password = password;
	}
	private Connection getConnection() throws ClassNotFoundException, SQLException
	{
		Class.forName(driver);
		return DriverManager.getConnection(url,user,password);
	}
	
	private Patient getPatient(ResultSet rset)
	{
		Patient patient=new Patient();
			
			try
		{
				patient.setName(rset.getString("name"));
				patient.setAge(rset.getInt("age"));
				patient.setDoctorName(rset.getString("doctor_name"));
				patient.setGender(Gender.valueOf(rset.getString("gender")));
				patient.setDateOfBirth(rset.getDate("date_of_birth").toLocalDate());
				
		} catch (SQLException e) {
			e.printStackTrace();
			}
return patient;
			
	}
	
	public void add(Patient patient) 
	{
		
		
		
		try
		{
			Connection cn=getConnection();
			
			String query="INSERT INTO patient (name,age,doctor_name,gender,date_of_birth) VALUES (?,?,?,?,?)";
			PreparedStatement stmt=cn.prepareStatement(query);
			stmt.setString(1,patient.getName());
			stmt.setInt(2,patient.getAge());
			stmt.setString(3,patient.getDoctorName());
			stmt.setString(4,patient.getGender().toString());
			stmt.setDate(5,Date.valueOf(patient.getDateOfBirth()));
			
			stmt.executeUpdate();
	        cn.close();
		
	}catch(Exception e)
		{
		e.printStackTrace();
		System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	    System.exit(0);
	 }
		System.out.println();
	 System.out.println("VALUES ARE SUCESSFULLY INSERTED");
	}
	
	
	public void display() 
	{
		try
		{
		Connection cn=getConnection();
		String query="SELECT * FROM patient  ";
		PreparedStatement st=cn.prepareStatement(query);
		
	      
        ResultSet rset=st.executeQuery();
        while(rset.next())
        {
        	

			Patient patient=getPatient(rset);
			System.out.println(patient);
			
        	
       	/* String pname=rset.getString("name");
       	 Integer age=rset.getInt("age");
       	 String doctor_name=rset.getString("doctor_name");
       	 String gender=rset.getString("gender");
       	 Date date_of_birth=rset.getDate("date_of_birth");
       	 
       	 System.out.println("\n");
       	 System.out.println("NAME :" +pname);
       	 System.out.println("AGE :" +age);
    	 System.out.println("DOCTOR NAME : " +doctor_name);
       	 System.out.println("GENDER :" +gender);
       	 System.out.println("DOB :" +date_of_birth);*/
       	 
        }
        	st.close();
        	rset.close();
        	cn.close();
		}catch(Exception e)
		{
		e.printStackTrace();
		System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	    System.exit(0);
	 }
		System.out.println();
		System.out.println("VALUES ARE SUCESSFULLY DISPLAYED");
	}

	
	
	public void search(String name)
	{
		try {
			Connection cn ;
			cn = getConnection();
			PreparedStatement preparedStatement = cn.prepareStatement("SELECT * FROM patient where name=?");
			preparedStatement.setString(1, name);
			ResultSet rset = preparedStatement.executeQuery();
			while (rset.next())

			{
				
					Patient patient=getPatient(rset);
					System.out.println(patient);
					}
			
	       	 System.out.println("\n");
	       	
			cn.close();



			} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			}


		
	}
	public void sort()
	{	try {
			Connection connection;
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM patient order by name");
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next())



			{
			Patient patient = getPatient(resultSet);
			System.out.println(patient);



			}
			resultSet.close();
			preparedStatement.close();
			connection.close();



			} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
			}
			System.out.println("Sort displayed");



			}

	public List<Patient> search(LocalDate date)
	{
		List<Patient>patientlist = new ArrayList<Patient>();
		try {
			Connection cn ;
			cn = getConnection();
			PreparedStatement preparedStatement = cn.prepareStatement("SELECT * FROM patient where date_of_birth=?");
			preparedStatement.setDate(1,Date.valueOf(date));
			ResultSet rset = preparedStatement.executeQuery();
			while (rset.next())

			{
				patientlist.add(getPatient(rset));
				//Patient patient=getPatient(rset);
				//System.out.println(patient);
			
			}
			cn.close();


			
			} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			}
		
	

		return (patientlist);
	
	}
	
	public void delete(String name)
	{
		
		try {
			Connection cn ;
			cn = getConnection();
			PreparedStatement preparedStatement = cn.prepareStatement("DELETE FROM patient where name=?");
			preparedStatement.setString(1, name);
			
			
			preparedStatement.executeUpdate();
			System.out.println("ELEMENT DELETED");
			
			cn.close();
			//display(); 
			
			



			} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			}

	}
	
	public void downloadCSVFile()throws IOException 
	
	{
		
		Connection connection;
		try {
		connection = getConnection();
		PreparedStatement pstmt = connection.prepareStatement("select * from patient");
		ResultSet resultSet = pstmt.executeQuery();
		FileWriter file = new FileWriter("download.csv");
		while (resultSet.next()) {

		String data =convertTocsv(getPatient(resultSet)).toString();
		file.write(data);
		}
		file.close();
		} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		
	}
	
	
	public void downloadJSONFile()throws IOException
				{
		
		Connection connection;
			
			
			
			try {
			connection = getConnection();
			PreparedStatement pstmt = connection.prepareStatement("select * from patient");
			ResultSet resultSet = pstmt.executeQuery();
			FileWriter file = new FileWriter("download1.json");
			StringJoiner join = new StringJoiner(",", "[", "]");
			while (resultSet.next()) {
			String data = join.add(convertTojson(getPatient(resultSet))).toString();
			file.write(data);
			
			
			
			}
			file.close();
			connection.close();
			} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}



}
		

	
	
	}



