package main;


import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Properties;
import java.util.Scanner;


public class Menu 
{
	

	public static void main(String[] args) throws IOException
	{
		Scanner sn=new Scanner(System.in);
		//String "";
		//PatientService  patientService=new PatientServiceImpl();
		FileReader reader=new FileReader("src/config.properties");
		Properties properties=new Properties();
		properties.load(reader);
		
		PatientService patientService=PatientServiceFactory.getPatientService(properties);
		
			int ch;
		//int count=0;
		do
		{
		System.out.println("\n 1. INSERT RECORDS \n 2.DISPLAY DETAILS \n 3.SEARCH USING NAME \n 4. SEARCH USIN DOB \n 5. SORTING \n 6. DOWNLOAD CSVFILE \n 7.DOWNLOAD AS JSON FILE \n.8. DELETION \n 9.EXIT \n");
		System.out.println();
		System.out.println("Enter your choice:");
		  ch=sn.nextInt();
		
		 Gender gender;
		 System.out.println();
		switch(ch)
		 {
		 case 1:
			 
			 System.out.println("YOU SELECTED CHOICE 1!!");
				
						System.out.println();
						System.out.println("================================");
						System.out.println("ENTER YOUR DETAILS: ");
						System.out.println();
						System.out.println("Enter your name  :");
					    String  name=sn.next();
					    System.out.println("Enter your age :");
						int age=sn.nextInt();
						System.out.println("Enter the doctors name :");
						String doctorName=sn.next();
						System.out.println("Enter your gender :");
					    gender=Gender.valueOf(sn.next().toUpperCase());
						System.out.println("Enter your dob :");
						String dob = sn.next();
						LocalDate date = LocalDate.parse(dob);
			            Patient patient=new Patient(name,age,doctorName,gender,date);
		                patientService.add(patient);
		                System.out.println();
		                break;
						
						 
		 case 2:
				
					
					   System.out.println("YOU SELECTED CHOICE 2!!");
					   System.out.println("-----------------------------");
					   System.out.println();
					   patientService.display();
					   break;
					   
		 case 3:
			 		   System.out.println("YOU SELECTED CHOICE 3!!");
			 		   System.out.println("-----------------------------");
			 		   System.out.println();
			 		   System.out.println("Enter the search element(name):");
			 		   String namee=sn.next();
			 		  System.out.println();
			 		   patientService.search(namee);
			 		  System.out.println();
			 		   break;
			 
		 case 4:
					   System.out.println("YOU SELECTED CHOICE 4!!!!!!!!!!!!");
					   System.out.println("-----------------------------");
					   System.out.println();
					   System.out.println("Enter Your dateOfBirth of birth in YYYY-MM-DD");
					   String searchDate = sn.next();
					   LocalDate date1= LocalDate.parse(searchDate);
					   patientService.search(date1);
					   System.out.println();
					  // patientService.display();
					   break;
			 
		 case 5:
			 		   System.out.println("YOU SELECTED CHOICE 5!!!!!!");
			 		   System.out.println("-----------------------------");
			 		   System.out.println();
			 		  System.out.println("THE SORTED LIST IS :");
			 		   patientService.sort();
			 		  System.out.println();
			 		   break;
			
		 case 6:
			 try
			 	{
				 System.out.println("YOU SELECTED CHOICE 6!!");
				 System.out.println("-----------------------------");
				 System.out.println();
				 System.out.println("CSV FILE IS DOWNLOADED!!!!");
				 patientService.downloadCSVFile();
				 System.out.println();
			 }catch(IOException e)
			 {
				 System.out.println("Exception");
				 e.printStackTrace();
			 }
			 break;
			 
		 case 7:
			 try
			 {
				 System.out.println("YOU SELECTED CHOICE 7!!");
				 System.out.println("-----------------------------");
				 System.out.println();
				 System.out.println();
				 System.out.println("DATAS ARE DOWNLOADED AS JSON!!!!!!!");
				 System.out.println("-----------------------");
			 	 patientService.downloadJSONFile();
			 	System.out.println();
			 }catch(IOException e)
			 {
				 System.out.println("Exception");
					e.printStackTrace();
			 }
			 break;
		 case 8:
			 	System.out.println("YOU SELECTED CHOICE 8!!");
			 	System.out.println("-----------------------------");
			 	System.out.println();
			 	System.out.println("Enter the name of patient to be deleted:");
			 	String name1=sn.next();
			 	patientService.delete(name1);
			 	System.out.println();
				break;
			 
		
		 case 9:
			 	System.out.println("YOU SELECTED CHOICE 9!!");
			 	System.out.println("-----------------------------");
			 	System.out.println();
			 	System.out.println("YOU HAVE EXITED!!!!");
			 	System.exit(ch);
			 
		 }
		
	}while(ch!=9);

}
}
